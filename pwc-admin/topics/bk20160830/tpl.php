<?php

require __DIR__.'/../common/start.php';

// テンプレート設定
$templateFile = '/admin/topics/tpl.html';

// データ取得
$model = new Model(TBL_TOPICS_TPL);
$data = h($model->find());

require __DIR__ . '/../common/display.php';
