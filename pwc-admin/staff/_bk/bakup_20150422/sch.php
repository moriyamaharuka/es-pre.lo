<?php

require __DIR__ . '/../common/start.php';

// テンプレート設定
$templateFile = '/admin/staff/sch.html';

// 1週間の取得
if (is_null($params['w'])) {
    $params['w']=1;
}
$week = getSchWeek($params['w']);

// 基本情報取得
$model = new Model(TBL_STAFF);
$sel = $model->select();

// IDがある場合はスタッフ単一
if ($params['id'] > 0) {
    $data = h($sel->fields('id, name')
                  ->where('id', $params['id'])
                  ->order('seq')
                  ->fetchAll());
} else {
    // IDがない場合は複数人取得
    $model = new Model(TBL_STAFF);
    $cnt = $sel->where('show_flg', SHOW_ON)
               ->fetchCount();
    // ページネーション生成
    // 改ページすると登録処理が走らない為1時的にコメントアウト 2015/04/16
    // $offset = pagination($cnt, PAGER_ADMIN_LIST);
    // IDがない場合は複数人取得
    $data = h($sel->fields('id, name')
                  ->where('show_flg', SHOW_ON)
                  ->where('rest_flg', SET_OFF)
                  ->order('seq')
                  // 改ページすると登録処理が走らない為1時的にコメントアウト 2015/04/16
                  // ->limit(PAGER_ADMIN_LIST)
                  // ->offset($offset)
                  ->fetchAll());
}

// サムネイル画像(1枚目) / スケジュール情報の取得
if (count($data)>0) {
    foreach ($data as $k => $v) {
        $data[$k]['photo'] = getStaffPhoto($v['id'], 1);
        $data[$k]['sch']   = getStaffSchedule($v['id'], $week, true);
    }
}

$smarty->assign('week', $week);

require __DIR__ . '/../common/display.php';
