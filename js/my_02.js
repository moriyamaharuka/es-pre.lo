;jQuery(function($){

	//スライドショー for ヘッドライン
	$('.headline ul').cycle({
		fx: "scrollHorz",
		speed: 1000,
		manualSpeed: 100
	});
	//スライドショー for ヘッドライン
		$('#section_covergirl .cast_box').cycle({
		speed: 2000,
		manualSpeed: 100
	});
	//スライドショー for メインビジュアル
		$('#mainvisual .inner').cycle({
		speed: 4000,
		manualSpeed: 100
	});

	var topBtn = $('#Page-Top');
	topBtn.hide();
	//スクロールが100に達したらボタン表示
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			topBtn.fadeIn();
		} else {
			topBtn.fadeOut();
		}
	});
	//スクロールしてトップ
	topBtn.click(function () {
		$('body,html').stop().animate({
			scrollTop: 0
		}, 500, 'linear');
		return false;
	});

	//体験漫画
		$('.flexslider').flexslider({
		slideshow: false,
		animation: "slide",
		slideshowSpeed: 8000
	});
});