;jQuery(function($){

	//スライドショー for ヘッドライン
	$('.headline ul').cycle({
		fx: "scrollHorz",
		speed: 1000,
		manualSpeed: 100
	});
	//スライドショー for ヘッドライン
		$('#section_covergirl .cast_box').cycle({
		speed: 2000,
		manualSpeed: 100
	});

	//スライドショー for メインビジュアル(Ver.flexslider)
	$('#mainvisual').flexslider({
		slideshow: true,
		animation: "fade",
		slideshowSpeed: 5000,
		directionNav: false,
		animationLoop: true,
		pauseOnAction: false,
		touch: true,
		video: true,
	});


	var topBtn = $('#Page-Top');
	topBtn.hide();
	//スクロールが100に達したらボタン表示
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			topBtn.fadeIn();
		} else {
			topBtn.fadeOut();
		}
	});
	//スクロールしてトップ
	topBtn.click(function () {
		$('body,html').stop().animate({
			scrollTop: 0
		}, 500, 'linear');
		return false;
	});

	//体験漫画
	$('#manga').flexslider({
		slideshow: false,
		animation: "slide",
		slideshowSpeed: 8000
	});

	// RSS

	var mySwiper = new Swiper('.swiper-container', {
		direction: 'vertical',
		// 自動再生
		autoplay: {
	    delay: 4000,
	  	},
		speed: 4000,
		//一度に表示する個数を選択
		slidesPerView: 3,
	});
});