<?php /* Smarty version 3.1.22-dev/9, created on 2019-06-04 19:24:31
         compiled from "/Applications/MAMP/htdocs/es-pre.lo/view/pc/common/head.html" */ ?>
<?php
/*%%SmartyHeaderCode:13918244955cf646df3e1140_56788012%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2f7254199320778fd44cd1a4797a81ca12924712' => 
    array (
      0 => '/Applications/MAMP/htdocs/es-pre.lo/view/pc/common/head.html',
      1 => 1559642365,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13918244955cf646df3e1140_56788012',
  'tpl_function' => 
  array (
  ),
  'variables' => 
  array (
    'page' => 0,
    'sp_dir_path' => 0,
    'mb_dir_path' => 0,
    'pc_dir_path' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.22-dev/9',
  'unifunc' => 'content_5cf646df4179e2_13723749',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5cf646df4179e2_13723749')) {
function content_5cf646df4179e2_13723749 ($_smarty_tpl) {
?>
<?php
$_smarty_tpl->properties['nocache_hash'] = '13918244955cf646df3e1140_56788012';
?>
<?php echo '<script'; ?>
 src="js/google_analytics.js"><?php echo '</script'; ?>
><!-- アナリティクス gtagである限りこの位置で記述すること -->
<meta charset="utf-8">
<title><?php echo (($tmp = @$_smarty_tpl->tpl_vars['page']->value['title'])===null||$tmp==='' ? '' : $tmp);?>
</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="description" content="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['page']->value['description'])===null||$tmp==='' ? '' : $tmp);?>
">
<meta name="keywords" content="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['page']->value['keywords'])===null||$tmp==='' ? '' : $tmp);?>
">
<meta name="viewport" content="width=960, user-scalable=yes">

<link rel="stylesheet" href="<?php echo @constant('ROOT_URL');?>
/css/style.css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"><!-- Webfont -->
<link rel="stylesheet" href="css/tabswitch.css"><!-- タブの切替 -->
<link rel="stylesheet" href="css/flexslider.css"><!-- 体験漫画 -->
<link rel="stylesheet" href="css/swiper.min.css"><!-- RSS -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" /><!-- 日付ピッカー -->
<link rel="shortcut icon" href="<?php echo @constant('IMAGES_URL');?>
/favicon.ico">
<link rel="apple-touch-icon" href="<?php echo @constant('IMAGES_URL');?>
/apple-touch-icon.png">
<link rel="alternate" media="only screen and (max-width: 640px)" href="<?php echo @constant('SHOP_URL');
echo $_smarty_tpl->tpl_vars['sp_dir_path']->value;?>
">
<link rel="alternate" media="handheld" href="<?php echo @constant('SHOP_URL');
echo $_smarty_tpl->tpl_vars['mb_dir_path']->value;?>
">
<link rel="canonical" href="<?php echo @constant('SHOP_URL');
echo $_smarty_tpl->tpl_vars['pc_dir_path']->value;?>
">
<?php echo '<script'; ?>
 src="js/jquery-1.11.2.min.js"><?php echo '</script'; ?>
><!-- jQuery -->
<?php echo '<script'; ?>
 src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="js/jquery.cycle.all.min.js"><?php echo '</script'; ?>
><!-- cycle -->
<?php echo '<script'; ?>
 src="js/tabswitch.js"><?php echo '</script'; ?>
><!-- タブの切替 -->
<?php echo '<script'; ?>
 src="js/jquery.flexslider-min.js"><?php echo '</script'; ?>
><!-- 体験漫画 -->
<?php echo '<script'; ?>
 src="js/swiper.min.js"><?php echo '</script'; ?>
><!-- RSS -->
<?php echo '<script'; ?>
 type="text/javascript" src="js/jquery.ui.datepicker-ja.js"><?php echo '</script'; ?>
><!-- 日付ピッカー -->
<?php echo '<script'; ?>
 src="js/my.js"><?php echo '</script'; ?>
>
<!--[if lt IE 9]>
	<?php echo '<script'; ?>
 src="http://html5shiv.googlecode.com/svn/trunk/html5.js"><?php echo '</script'; ?>
>
<![endif]-->






<?php }
}
?>