<?php /* Smarty version 3.1.22-dev/9, created on 2019-06-04 19:24:31
         compiled from "/Applications/MAMP/htdocs/es-pre.lo/view/pc/common/footer.html" */ ?>
<?php
/*%%SmartyHeaderCode:14681432315cf646df596b83_66401836%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b69be5331806a0a5a69f804425ea8fa2a86cdddb' => 
    array (
      0 => '/Applications/MAMP/htdocs/es-pre.lo/view/pc/common/footer.html',
      1 => 1559642364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14681432315cf646df596b83_66401836',
  'tpl_function' => 
  array (
  ),
  'variables' => 
  array (
    'html' => 0,
    'page' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.22-dev/9',
  'unifunc' => 'content_5cf646df677626_37268227',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5cf646df677626_37268227')) {
function content_5cf646df677626_37268227 ($_smarty_tpl) {
?>
<?php
$_smarty_tpl->properties['nocache_hash'] = '14681432315cf646df596b83_66401836';
?>
<footer>
	<div class="inner">
		<ul class="nav">
			<li><a href="<?php echo @constant('ROOT_URL');?>
/top.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['top'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/system.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['system'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/staff.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['staff'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/schedule.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['schedule'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/hotel.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['hotel'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/recruit.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['recruit'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/magazine.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['magazine'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/link.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['link'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/enquete.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['enquete'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/reserve.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['reserve'];?>
</a></li>
		</ul>
		<ul class="nav">
			<li><a href="<?php echo @constant('ROOT_URL');?>
/ranking.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['ranking'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/contact.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['contact'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/info.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['info'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/play.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['play'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/manga.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['manga'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/qa.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['qa'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/access.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['access'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/concept.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['concept'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/credit.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['credit'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/schedule_week.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['schedule_week'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/event.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['event'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/impressions.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['impressions'];?>
</a></li>
		</ul>
		<!-- ロゴ -->
		<p class="logo"><a href="#"><img src="<?php echo @constant('IMAGES_URL');?>
/logo.png" alt="<?php echo $_smarty_tpl->tpl_vars['page']->value['h2'];?>
" width="160"></a></p>
		<!-- ロゴ -->
		<!-- 電話番号＆営業時間 -->
		<ul class="info_box">
			<li><a href="tel:<?php echo @constant('SHOP_TEL');?>
"><img src="<?php echo @constant('IMAGES_URL');?>
/img_tel.png" alt="電話番号：<?php echo @constant('SHOP_TEL');?>
" width="250"></a></li>
			<li><img src="<?php echo @constant('IMAGES_URL');?>
/img_open.png" alt="営業時間：<?php echo @constant('SHOP_OPEN');?>
" width="250"></li>
		</ul>
		<!-- 電話番号＆営業時間 -->
		<p>
			&copy;&nbsp;
			<?php echo '<script'; ?>
 type="text/javascript">
				$y=2016;$ny=new Date().getFullYear();
				document.write($ny>$y?$y+'-'+$ny: $y);
			<?php echo '</script'; ?>
>
			&nbsp;<?php echo @constant('SHOP_NAME');?>

		</p>
	</div>
</footer><?php }
}
?>