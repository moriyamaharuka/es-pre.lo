<?php /* Smarty version 3.1.22-dev/9, created on 2019-06-04 19:24:31
         compiled from "/Applications/MAMP/htdocs/es-pre.lo/view/pc/common/header.html" */ ?>
<?php
/*%%SmartyHeaderCode:14098613575cf646df41cda0_86629924%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '71686649b11ffd3a12be3733881df2fc5d8bc443' => 
    array (
      0 => '/Applications/MAMP/htdocs/es-pre.lo/view/pc/common/header.html',
      1 => 1559642365,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14098613575cf646df41cda0_86629924',
  'tpl_function' => 
  array (
  ),
  'variables' => 
  array (
    'page' => 0,
    'headline' => 0,
    'v' => 0,
    'form' => 0,
    'html' => 0,
    'bread' => 0,
    'value' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.22-dev/9',
  'unifunc' => 'content_5cf646df53c384_67988304',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5cf646df53c384_67988304')) {
function content_5cf646df53c384_67988304 ($_smarty_tpl) {
?>
<?php
$_smarty_tpl->properties['nocache_hash'] = '14098613575cf646df41cda0_86629924';
?>
<header>
	<div class="inner">
		<div id="tmp_sw">
			<p>テンプレート：</p>
			<form id="form_sw" action="" method="post">
			<select name="tmp_sw">
				<option value="1" <?php if ($_SESSION['tmp_sw']==1) {?>selected<?php }?>>1. メニュー横/1カラム</option>
				<option value="2" <?php if ($_SESSION['tmp_sw']==2) {?>selected<?php }?>>2. メニュー横/2カラム</option>
				<option value="3" <?php if ($_SESSION['tmp_sw']==3) {?>selected<?php }?>>3. メニュー縦/2カラム</option>
			</select>
			</form>
		</div>
		<?php echo '<script'; ?>
>
			$(function(){
				$('[name=tmp_sw]').change(function(){
					$('#form_sw').submit();
				});
			});
		<?php echo '</script'; ?>
>
		
		<h1><?php echo $_smarty_tpl->tpl_vars['page']->value['h1'];?>
</h1>
		<!-- ロゴ -->
		<h2 class="logo"><a href="/"><img src="<?php echo @constant('IMAGES_URL');?>
/logo.png" alt="<?php echo $_smarty_tpl->tpl_vars['page']->value['h2'];?>
" width="290"></a></h2>
		<!-- ロゴ -->
		<!-- 電話番号＆営業時間 -->
		<ul class="info_box">
			<li><a href="tel:<?php echo @constant('SHOP_TEL');?>
"><img src="<?php echo @constant('IMAGES_URL');?>
/img_tel.png" alt="電話番号:<?php echo @constant('SHOP_TEL');?>
"></a></li>
			<li><img src="<?php echo @constant('IMAGES_URL');?>
/img_open.png" alt="営業時間:<?php echo @constant('SHOP_OPEN');?>
"></li>
		</ul>
		<!-- 電話番号＆営業時間 -->
		<!-- ヘッドラインニュース -->
		<div class="headline">
			<ul>
			<?php
$_from = $_smarty_tpl->tpl_vars['headline']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['v']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
$foreachItemSav = $_smarty_tpl->tpl_vars['v'];
?>
				<?php if ($_smarty_tpl->tpl_vars['form']->value['line']['hex'][$_smarty_tpl->tpl_vars['v']->value['color_flg']]!='') {?>
				<li><span style="color:<?php echo $_smarty_tpl->tpl_vars['form']->value['line']['hex'][$_smarty_tpl->tpl_vars['v']->value['color_flg']];?>
;"><?php echo $_smarty_tpl->tpl_vars['v']->value['name'];?>
</span></li>
				<?php } else { ?>
				<li><?php echo $_smarty_tpl->tpl_vars['v']->value['name'];?>
</li>
				<?php }?>
			<?php
$_smarty_tpl->tpl_vars['v'] = $foreachItemSav;
}
?>
			</ul>
		</div>
		<!-- ヘッドラインニュース -->
	</div>
	<!-- グローバルメニュー -->
	<nav>
		<ul class="inner">
			<li><a href="<?php echo @constant('ROOT_URL');?>
/top.php"  id="gnav_01"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['top'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/system.php"  id="gnav_02"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['system'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/staff.php"  id="gnav_03"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['staff'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/schedule.php"  id="gnav_04"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['schedule'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/hotel.php"  id="gnav_05"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['hotel'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/recruit.php"  id="gnav_06"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['recruit'];?>
</a></li>
			<!-- <li><a href="<?php echo @constant('ROOT_URL');?>
/magazine.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['magazine'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/link.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['link'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/enquete.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['enquete'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/reserve.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['reserve'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/ranking.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['ranking'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/contact.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['contact'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/info.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['info'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/play.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['play'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/manga.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['manga'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/qa.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['qa'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/access.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['access'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/concept.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['concept'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/credit.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['credit'];?>
</a></li>
			<li><a href="<?php echo @constant('ROOT_URL');?>
/schedule_week.php"><?php echo $_smarty_tpl->tpl_vars['html']->value['name']['schedule_week'];?>
</a></li> -->
		</ul>
	</nav>
	<!-- //グローバルメニュー -->
</header>

<!-- パンくず -->
<div class="breadcrumbs">
	<ul class="inner">
	<?php
$_from = $_smarty_tpl->tpl_vars['bread']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['value']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$foreachItemSav = $_smarty_tpl->tpl_vars['value'];
?>
		<li>
		<?php if ($_smarty_tpl->tpl_vars['value']->value['url']!='') {?>
			<a href="<?php echo $_smarty_tpl->tpl_vars['value']->value['url'];?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value['title'];?>
</a>
		<?php } else { ?>
			<?php echo $_smarty_tpl->tpl_vars['value']->value['title'];?>

		<?php }?>
		</li>
	<?php
$_smarty_tpl->tpl_vars['value'] = $foreachItemSav;
}
?>
	</ul>
</div>
<!-- //パンくず --><?php }
}
?>