<?php

/*
    出勤ページ（週間）
 */

$week = getSchWeek($params['w']);
$day = getParamDays();

$db = Db::factory();

$query = 'select st.*, st.name, sc.allow_mode,';
$query .= ' case when sc.allow_mode = 0 or sc.allow_mode = 1 then from_unixtime(sc.going_time, \'%H:%i\')';
$query .= ' else \'\' end as going,';
$query .= ' case when sc.allow_mode = 0 then from_unixtime(sc.leave_time, \'%H:%i\')';
$query .= ' when sc.allow_mode = '.SCH_TYPE_LAST.' then \'LAST\'';
$query .= ' else \'\' end as `leave`';
$query .= ' from '.TBL_STAFF.' st';
$query .= ' inner join '.TBL_STAFF_SCH.' sc';
$query .= ' on st.id = sc.staff_id and sc.date = \''.$week[$day-1].'\'';
$query .= ' where st.show_flg = '.SHOW_ON;
$query .= ' and st.rest_flg != '.SHOW_ON;
$query .= ' order by st.seq desc';

if (0 <= WORK_END_1 && WORK_END_1 <= SHOP_TODAY) {
    $leave_hour = WORK_END_1 + 23;
} else {
    $leave_hour = WORK_END_1 -1;
}

foreach ($db->query($query) as $key => $row) {
    if ($row['allow_mode'] != SCH_TYPE_REST) {
        $staffs[$key] = $row;
        // TEL確認 or スケジュール未登録
        if ($row['allow_mode'] == SCH_TYPE_TEL || $row['allow_mode'] == '') {
            $staffs[$key]['going_sta'] = (int)WORK_START_1;
            $staffs[$key]['going_end'] = (int)WORK_START_2;
            $staffs[$key]['leave_sta'] = (int)$leave_hour;
            $staffs[$key]['leave_end'] = (int)WORK_END_2;

        } elseif ($row['allow_mode'] == SCH_TYPE_LAST) {
            $temp = explode(':', $row['going']);
            if (0 <= $temp[0] && $temp[0] <= SHOP_TODAY) {
                $going_hour = $temp[0] + 24;
            } else {
                $going_hour = $temp[0];
            }
            $staffs[$key]['going_sta'] = (int)$going_hour;
            $staffs[$key]['going_end'] = (int)$temp[1];
            $staffs[$key]['leave_sta'] = (int)$leave_hour;
            $staffs[$key]['leave_end'] = (int)WORK_END_2;
        } else {
            $temp1 = explode(':', $row['going']);
            $temp2 = explode(':', $row['leave']);
            if (0 <= $temp1[0] && $temp1[0] <= SHOP_TODAY) {
                $going_hour = $temp1[0] + 24;
            } else {
                $going_hour = $temp1[0];
            }
            if (0 <= $temp2[0] && $temp2[0] <= SHOP_TODAY) {
                $leave_hour = $temp2[0] + 23;
            } else {
                $leave_hour = $temp2[0] -1;
            }
            if ((int)$temp2[1]>0) {
                $leave_hour++;
            }
            $staffs[$key]['going_sta'] = (int)$going_hour;
            $staffs[$key]['going_end'] = (int)$temp1[1];
            $staffs[$key]['leave_sta'] = (int)$leave_hour;
            $staffs[$key]['leave_end'] = (int)$temp2[1];
        }
        $staffs[$key]['leave_end2'] = 0;
        if ($staffs[$key]['leave_end'] != 0) {
            $staffs[$key]['leave_end2'] = 60 - $staffs[$key]['leave_end'];
        }
        // サムネイル画像(1枚目)取得
        $staffs[$key]['photo'] = getStaffPhoto($row['id'], 1);
    }
    // 予約状況取得
    $query2 = ' select id, from_unixtime(going_time,\'%H:%i\') as going, from_unixtime(leave_time, \'%H:%i\') as `leave` from '.TBL_STAFF_RESERVE;
    $query2 .= ' where staff_id = '.$row['id'];
    $query2 .= ' and date = \''.$week[$day-1].'\'';
    $query2 .= ' order by going_time, id';

    foreach ($db->query($query2) as $key2 => $row2) {

        $temp1 = explode(':', $row2['going']);
        $temp2 = explode(':', $row2['leave']);

        if (0 <= $temp1[0] && $temp1[0] <= SHOP_TODAY) {
            $going_hour = $temp1[0] + 24;
        } else {
            $going_hour = $temp1[0];
        }
        $going_min = $temp1[1];

        if (0 <= $temp2[0] && $temp2[0] <= SHOP_TODAY) {
            $leave_hour = $temp2[0] + 24;
        } else {
            $leave_hour = $temp2[0];
        }
        $leave_min = $temp2[1];

        $sta = 0;
        $sta_1 = ($going_hour-$staffs[$key]['going_sta'])*6+1;
        $sta_2 = ($going_min - $staffs[$key]['going_end'])/10;

        $sta = $sta_1 + $sta_2 + ($staffs[$key]['going_end']/10);

        $join = (($leave_hour-$going_hour)*6) + (($leave_min-$going_min)/10);

        $reserve[$row['id']][] = array(
            'sta' => $sta,
            'join' => $join,
            'end' => $sta + $join - 1,
            'time' => $row2['going'].'-'.$row2['leave'],
        );
    }

}

var_dump($reserve);

$smarty->assign('week', getSchWeek());	// 週間
$smarty->assign('staff', $staffs);
$smarty->assign('reserve', $reserve);